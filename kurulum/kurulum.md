## Giris

Zephyr projesinin Linux tabanli bir isletim sistemi iceren makinaya kurulum islemleri projenin dokuman
sayfasinda yer almaktadir. Bu dokuman proje sayfasinda anlatilan dokumana paralel olarak ilerleyecek ve
bir kac noktada onemli ipuclari icerecektir.

Zephyr 1.13.99 versiyonu asagida listelenmis isletim sistemlerinde test edilmistir.
 - Ubuntu 16.04 LTS 64-bit
 - Fedora 25 64-bit
 - Clear Linux
 - Arch Linux

Bu dokuman anlatimi 4.17.17 kerneline sahip Fedora 27 64-bit sistemi uzerinde yapilmaktadir. 

## Kurulum

Oncelikle github sayfasinda yer alan zephyr rtos kaynak kodlarini indirilir.

`git clone https://github.com/zephyrproject-rtos/zephyr.git`

Ardindan, toolchain dosyalarini iceren Zephyr-SDK projesinin indirilmesi gerekmektedir. Bu islem
Arch Linux isletim sistemine sahip makinalarda AUR uzerinden gerceklestirilebilirken, yukaridaki
diger isletim sistemleri icin sirasiyla;

**Ubuntu;**
 - `sudo apt-get update`
 - `sudo apt-get upgrade`
 - `sudo apt-get install --no-install-recommends git cmake ninja-build gperf ccache doxygen dfu-util
   device-tree-compiler python3-ply python3-pip python3-setuptools python3-wheel xz-utils file make
   gcc-multilib autoconf automake libtool librsvg2-bin texlive-latex-base texlive-latex-extra
   latexmk texlive-fonts-recommended`
 
**Fedora;**
 - `sudo dnf upgrade`
 - `sudo dnf group install "Development Tools" "C Development Tools and Libraries"`
 - `sudo dnf install git cmake ninja-build gperf ccache doxygen dfu-util dtc python3-pip python3-ply
   python3-yaml dfu-util dtc python3-pykwalify glibc-devel.i686 libstdc++-devel.i686 autoconf automake
   libtool texlive-latex latexmk texlive-collection-fontsrecommended librsvg2-tools`

**Clear Linux;**
 - `sudo swupd update`
 - `sudo swupd bundle-add c-basic dev-utils dfu-util dtc os-core-dev python-basic python3-basic texlive`

komutlari calistirilmalidir. Bu komutlar Zephyr icin gerekli olan gelistirme paketlerinin kurulumunu gerceklestirir.
Paketlerin kurulumu tamamlandiktan sonra;

 - `cd ~/zephyr`  # or to your directory where zephyr is cloned
 - `pip3 install --user -r scripts/requirements.txt`

komutlari calistirilir. Bu komutlar zephyr icerisinde tanimlanmis Python scriptler icin gerekli olan Python modullerinin
kurulumunu saglar.

Zephyr, proje build surecini yonetmek icin CMake kullanir. CMake ise _generator_ olarak _make_ ve _ninja_ kullanir.
Zephyr projesindeki bir cok ornek _ninja_ aracini tercih eder fakat istege bagli olarak farkli araclar kullanilabilir.
*CMake versiyonunun 3.8.2 veya daha ust olmasi gerekmektedir.*

Zephyr SDK dosyasinin indirilmesi icin asagidaki komut calistirilir.

`wget https://github.com/zephyrproject-rtos/meta-zephyr-sdk/releases/download/0.9.3/zephyr-sdk-0.9.3-setup.run`

Indirilen SDK dosyasi

`sh zephyr-sdk-0.9.3-setup.run`

komutu ile calistirilir. Kurulum dizinin default olarak /opt/zephyr-sdk dizinidir, ancak bu dizin root kullanicisina
ait oldugu icin kurulum dizini olarak /home/<user> tercih edilmesi, proje build islemlerinin root yetkisine sahip
olmadan yapilmasini saglar.

Zephyr ve SDK uygulamalari indirilip kurulduktan sonra NRF52 MCU'na sahip bir board ile calistigimiz icin nrfjprog
ve SEGGER JLink uygulamalarinin kurulmasi gerekmektedir. nrfjprog uygulamasi Nordic firmaasi tarafindan gelistirilmis
bir uygulamadir. Hazirlanmis zephyr.hex kernel dosyasinin MCU'na yuklenmesi, halihazirda MCU icerisindeki kernelin
silinmesini, MCU'nun yeniden baslatilmasi ve benzeri komutlari calistirir. SEGGER JLink ise nrfjprog uygulamasinin
JLink donanimi uzerinden calismasini saglayan driver yazilimidir.

nrfjprog, https://www.nordicsemi.com/eng/nordic/Products/nRF51822/nRF5x-Command-Line-Tools-Linux64/51386 adresinden
SEGGER JLink ise https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack adresinden indirilebilir.

Calismalar Fedora 27 uzerinde yapildigindan dolayi SEGGER JLink icin 64-bit RPM paketi veya direk kaynak kod
indirilebilir.

nrfjprog indirilip bir dizine acildiktan sonra PATH degiskenine bu dizin eklenmelidir. Bu islemi shell acilisina
baglamak icin /home/<user>/.bashrc dosyasinin sonuna

 - `PATH=$PATH:$HOME/nrfjprog`
 - `export PATH`

komutlari eklenir. Yukaridaki ornek HOME dizinine acilmis nrfjprog ornegi icindir.

SEGGER JLink uygulamasi yuklendikten sonra JLink ve board birbirlerine ve bilgisayara baglandiktan sonra /dev dizini
altinda ttyUSB0 dosyasi olusacaktir.

## Hello World

Kurulum islemi tamamlandiktan sonra cevresel degiskenleri tanimlamanin iki yolu vardir. Birinci metot /home/<user>
dizini altindaki .bashrc dosyasinin sonuna;

 - `export ZEPHYR_BASE=/home/<user>/zephyr`
 - `export ZEPHYR_TOOLCHAIN_VARIANT=zephyr`
 - `export ZEPHYR_SDK_INSTALL_DIR=/home/<user>/zephyr-sdk`

komutlari eklemektir. Bu sayade her shell oturumu acildiginda zephyr ile alakali tum cevresel degiskenler ayarlanir.

Ikinci metot ise /home/<user> dizini altinda .zephyrrc adinda bir dosya olusturup icerisine;

 - `export ZEPHYR_TOOLCHAIN_VARIANT=zephyr`
 - `export ZEPHYR_SDK_INSTALL_DIR=/home/<user>/zephyr-sdk`

komutlari eklemek ve ardindan /home/<user>/zephyr dizinine giderek

`source zephyr-env.sh`

komutunu calistirmaktir. Birinci metot shell acildiginda otomatik gerceklesecektir. Ikinci durum ise her proje oncesinde
bir kereye mahsus yapilmak zorundadir.

Cevresel degiskenler ayarlandiktan sonra zephyr dizini icerisindeki sample klasoru altinda hello world adindaki ornek
proje dizinine girilir. Sirasi ile;

 - `mkdir build && cd build`
 - `cmake -GNinja -DBOARD=<board name> ../`
 - `ninja`
 - `ninja flash`

komutlari calistirilir. `ninja flash` komutu hazirlanan kernelin MCU'nun flash birimine yuklenmesini saglar. Bunun icin
once flash icerisini tamamiyle temizler, kerneli kopyalar ve son olarak MCU'ya reset komutu gonderir. Reset komutu
default olarak softreset komutudur, bunun icin board'a ait reset pininin gerekli baglantilarinin yapilmis olmasi
gerekmektedir. Aksi takdirde MCU reset edilemez ve uygulama baslamaz. Zephyr tarafindan listelenmis board'lar disinda
bir board ile calisildigindan `ninja flash` komutunun calistirdigi Python betik dosyasinda ufak bir guncelleme
yapilmistir.

`
if self.softreset:
   commands.append(['nrfjprog', '--reset', '-f', self.family, '--snr', board_snr])
else:
   commands.append(['nrfjprog', '--pinreset', '-f', self.family, '--snr', board_snr])
`
kod parcasindaki else bloguna

`commands.append(['nrfjprog', '--reset', '-f', self.family, '--snr', board_snr])`

kod parcasi eklenmistir.

`ninja flash` komutuna alternatif olarak,

 - `nrfjprog --eraseall -f nrf52`
 - `nrfjprog --program zephyr/zephyr.hex -f nrf52`
 - `nrfjprog --reset -f nrf52`

komutlari sirasi ile calistirilabilir.