#include "vcu.h"

//! bluetooth data
static const struct bt_data ad[] = {
  BT_DATA_BYTES( BT_DATA_FLAGS, ( BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR ) ),
  BT_DATA_BYTES( BT_DATA_UUID128_ALL,
		 0x51, 0x72, 0x69, 0x6d,
		 0x61, 0x72, 0x79, 0x53,
		 0x65, 0x72, 0x76, 0x69,
		 0x63, 0x65, 0x49, 0x64 )
  
};

/// @brief function that is called when the connection is established
/// @param conn - connection reference
/// @param err - possible connection error
/// @return -
static void connected( struct bt_conn *conn, u8_t err ) {
  if( err ) {
    printk( "Connection failed (err %u)\n", err );
  }
  else {
    printk( "Connected\n" );
  }

  return;
}


/// @brief function that is called when the disconnection is established
/// @param conn - connection reference
/// @param reason - possible disconnection reason
/// @return -
static void disconnected( struct bt_conn *conn, u8_t reason ) {
  printk( "Disconnected (reason %u)\n", reason );
  return;
}

//! connection callbacks
static struct bt_conn_cb conn_callbacks = {
  .connected = connected,
  .disconnected = disconnected
};

/// @brief function that is called when bluetooth is ready
/// @param err - possible error
/// @return -
static void bt_ready( int err ) {
  if( 0 != err ) {
    printk( "Bluetooth init failed (err %d)\n", err );
    return;
  }

  printk( "Bluetooth initialized\n" );

  // initialize weapon control unit
  initVCU();
  
  // start advertising
  err = bt_le_adv_start( BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE( ad ), NULL, 0 );

  if( err ) {
    printk( "Advertising failed to start (err %d)\n", err );
  }
  else {
    printk("Advertising successfully started\n" );
  }
  
  return;
}

/// @brief function that is called when authorize passkey is necessary
/// @param conn - connection reference
/// @param passkey - connection password
/// @return -
static void auth_passkey_display( struct bt_conn *conn, unsigned int passkey ) {
  char addr[ BT_ADDR_LE_STR_LEN ] = { 0x00 };

  // get connection address, then convert it to string
  bt_addr_le_to_str( bt_conn_get_dst( conn ), addr, sizeof( addr ) );

  printk( "Passkey for %s: %06u\n", addr, passkey );

  return;
}

/// @brief function that is called when authorize cancelled
/// @param conn - connection referencne
/// @return -
static void auth_cancel( struct bt_conn *conn ) {
  char addr[ BT_ADDR_LE_STR_LEN ] = { 0x00 };

  // get connection address, then convert it to string
  bt_addr_le_to_str( bt_conn_get_dst( conn ), addr, sizeof( addr ) );

  printk( "Pairing cancelled: %s\n", addr );

  return;
}

//! authorize callback structure instance
static struct bt_conn_auth_cb auth_cb_display = {
  .passkey_display = auth_passkey_display,
  .passkey_entry = NULL,
  .cancel = auth_cancel,
};

// entry point
void main( void ) {
  int err = 0;
  
  err = bt_enable( bt_ready );
  
  if( 0 != err ) {
    printk( "Bluetooth init failed (err %d)\n", err );
    return;
  }

  // register connected and disconnected callbacks
  bt_conn_cb_register( &conn_callbacks );

  // register authorize callbacks
  err = bt_conn_auth_cb_register( &auth_cb_display );

  if( 0 != err ) {
    printk( "Auth connection callback is not registered (err %d)\n", err );
    return;
  }
  
  while( 1 ) {
    k_sleep( 100 );
  }

  return;
}
