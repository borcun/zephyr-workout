/** @file vcu_util.h
 *  @brief Vest Control Unit Utility Header
 */

#ifndef VEST_CONTROL_UNIT_UTIL_H
#define VEST_CONTROL_UNIT_UTIL_H

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <misc/printk.h>
#include <misc/byteorder.h>
#include <zephyr.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <board.h>
#include <device.h>
#include <gpio.h>

//! OFF state of vest control unit
#define VCU_OFF    ( 0x00 )
//! LED ON state of vest control unit
#define VCU_LED_ON ( 0x01 )
//! motor ON state of vest control unit
#define VCU_MTR_ON ( 0x02 )
//! buzzier ON state of vest control unit
#define VCU_BZR_ON ( 0x04 )

//! VCU LED GPIO pin
#define VCU_LED_PIN ( 0x05 )
//! VCU motor GPIO pin
#define VCU_MTR_PIN ( 0x04 )
//! VCU buzzier GPIO pin
#define VCU_BZR_PIN ( 0x02 )

//! stack size of IO threads
#define STACK_SIZE ( 256 )
//! priority value of IO threads
#define PRIORITY ( 10 )

//! warning period in ms
#define VCU_WARNING_PERIOD ( 3000 )

//! GPIO response period in ms
#define VCU_RESPONSE_PERIOD ( 100 )

//! led port
#define LED_PORT ( "GPIO_0" )

// vest primary UUID
#define BT_UUID_VEST  BT_UUID_DECLARE_128( 0x51, 0x72, 0x69, 0x6d, \
					   0x61, 0x72, 0x79, 0x53, \
					   0x65, 0x72, 0x76, 0x69, \
					   0x63, 0x65, 0x49, 0x64 )

// vest IO UUID
#define BT_UUID_VEST_IO  BT_UUID_DECLARE_128( 0x51, 0x72, 0x69, 0x6d, \
					      0x61, 0x72, 0x79, 0x53, \
					      0x65, 0x72, 0x76, 0x69, \
					      0x63, 0x65, 0x49, 0x65 )


#endif
