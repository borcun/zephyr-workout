#include "vcu.h"

//! vcu service
struct bt_gatt_service vcu_svc = BT_GATT_SERVICE( vcu_attrs );

s16_t initVCU( void ) {
  int err = 0;
  
  led_state = 1;
  mtr_state = 1;
  bzr_state = 1;

  gpio_dev = device_get_binding( LED_PORT );

  if( NULL == gpio_dev ) {
    printk( "GPIO device not binding\n" );
    return -1;
  }

  if( 0 != bt_set_name( "VCU 1" ) ) {
    printk( "VCU BLE name not set\n" );
    return -1;
  }

  err = gpio_pin_configure( gpio_dev, VCU_LED_PIN, GPIO_DIR_OUT ); 
  if( 0 != err ) {
    printk( "LED pin is not configured (err %d)\n", err );
  }

  err = gpio_pin_configure( gpio_dev, VCU_MTR_PIN, GPIO_DIR_OUT );
  if( 0 != err ) {
    printk( "Motor pin is not configured (err %d)\n", err );
  }

  err = gpio_pin_configure( gpio_dev, VCU_BZR_PIN, GPIO_DIR_OUT );
  if( 0 != err ) {
    printk( "Buzzier pin is not configured (err %d)\n", err );
  }
  
  return bt_gatt_service_register( &vcu_svc );
}

ssize_t readVCU( struct bt_conn *conn,
	       const struct bt_gatt_attr *attr,
	       void *buf,
	       u16_t len,
	       u16_t offset )
{
  ssize_t res = bt_gatt_attr_read( conn,
				   attr,
				   buf,
				   len,
				   offset,
				   &vcu_data,
				   sizeof( vcu_data ) );

  if( res < 0 ) {
    printk( "Attribute read operation failed" );
    return res;
  }

  printk( "The central queried vest data state\n" );
  
  if( VCU_OFF == vcu_data ) {
    printk( " [LED]: OFF\n" );
    printk( " [MTR]: OFF\n" );
    printk( " [BZR]: OFF\n" );
  }
  else {
    if( VCU_LED_ON == ( VCU_LED_ON & vcu_data ) ) {
      printk( " [LED]: ON\n" );
    }
    else {
      printk( " [LED]: OFF\n" );
    }

    if( VCU_MTR_ON == ( VCU_MTR_ON & vcu_data ) ) {
      printk( " [MTR]: ON\n" );
    }
    else {
      printk( " [MTR]: OFF\n" );
    }

    if( VCU_BZR_ON == ( VCU_BZR_ON & vcu_data ) ) {
      printk( " [BZR]: ON\n" );
    }
    else {
      printk( " [BZR]: OFF\n" );
    }
  }

  printk( "\n" );
  
  return res;
}


ssize_t writeVCU( struct bt_conn *conn,
		const struct bt_gatt_attr *attr,
		const void *buf,
		u16_t len,
		u16_t offset,
		u8_t flags )
{
  if( NULL == buf || 0 == len ) {
    return 0;
  }
  
  memcpy( &vcu_data, buf, len );

  /// @attention VCU_OFF may be used for catastrophic situations
  if( VCU_OFF == vcu_data ) {
    atomic_set( &emit_led,  ATOMIC_INIT( 0 ) );
    atomic_set( &act_motor, ATOMIC_INIT( 0 ) );
    atomic_set( &trig_bzr,  ATOMIC_INIT( 0 ) );
  }
  else {
    printk( "The central updated vest data state\n" );
    
    if( VCU_LED_ON == ( VCU_LED_ON & vcu_data ) ) {
      printk( " [LED]: ON\n" );
      atomic_inc( &emit_led );
    }
    else {
      printk( " [LED]: OFF\n" );
    }

    if( VCU_MTR_ON == ( VCU_MTR_ON & vcu_data ) ) {
      printk( " [MTR]: ON\n" );
      atomic_inc( &act_motor );     
    }
    else {
      printk( " [MTR]: OFF\n" );
    }

    if( VCU_BZR_ON == ( VCU_BZR_ON & vcu_data ) ) {
      printk( " [BZR]: ON\n" );
      atomic_inc( &trig_bzr );
    }
    else {
      printk( " [BZR]: OFF\n" );
    }

    printk( "\n" );
  }
  
  return len;
}

void emitter( void ) {
  printk( "Emitter thread is running\n" );
  
  while( led_state ) {
    if( emit_led ) {
      atomic_dec( &emit_led );
      
      gpio_pin_write( gpio_dev, VCU_LED_PIN, 1 );
      k_sleep( VCU_WARNING_PERIOD );
      gpio_pin_write( gpio_dev, VCU_LED_PIN, 0 );
    }
    
    k_sleep( VCU_RESPONSE_PERIOD );
  }

  printk( "Emitter thread is done\n" );
  
  return;
}

void actuator( void ) {
  printk( "Actuator thread is running\n" );
  
  while( mtr_state ) {
    if( act_motor ) {
      atomic_dec( &act_motor );
      
      gpio_pin_write( gpio_dev, VCU_MTR_PIN, 1 );
      k_sleep( VCU_WARNING_PERIOD );
      gpio_pin_write( gpio_dev, VCU_MTR_PIN, 0 );
    }
    
    k_sleep( VCU_RESPONSE_PERIOD );
  }

  printk( "Actuator thread is done\n" );
  
  return;
}

void buzzier( void ) {
  printk( "Buzzier thread is running\n" );

  while( bzr_state ) {
    if( trig_bzr ) {
      atomic_dec( &trig_bzr );

      gpio_pin_write( gpio_dev, VCU_BZR_PIN, 1 );
      k_sleep( VCU_WARNING_PERIOD );
      gpio_pin_write( gpio_dev, VCU_BZR_PIN, 0 );
    }
    
    k_sleep( VCU_RESPONSE_PERIOD );
  }

  printk( "Buzzier thread is done\n" );
  
  return;
}
  

// led on/off thread
K_THREAD_DEFINE( led_thread,
		 STACK_SIZE,
		 emitter,
		 NULL,
		 NULL,
		 NULL,
		 PRIORITY,
		 0,
		 K_NO_WAIT);

// motor on/off thread
K_THREAD_DEFINE( mtr_thread,
		 STACK_SIZE,
		 actuator,
		 NULL,
		 NULL,
		 NULL,
		 PRIORITY,
		 0,
		 K_NO_WAIT);

// buzzier on/off thread
K_THREAD_DEFINE( bzr_thread,
		 STACK_SIZE,
		 buzzier,
		 NULL,
		 NULL,
		 NULL,
		 PRIORITY,
		 0,
		 K_NO_WAIT);
