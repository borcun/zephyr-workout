/** @file vcu.h
 *  @brief Vest Control Unit
 */

#ifndef VEST_CONTROL_UNIT_H
#define VEST_CONTROL_UNIT_H

#include "vcu_util.h"

#ifdef __cplusplus
extern "C" {
#endif
  //! vest control unit data
  static u8_t vcu_data;
   
  //! led emitter flag
  static atomic_t emit_led = ATOMIC_INIT( 0 ); 

  //! motor actuator flag
  static atomic_t act_motor = ATOMIC_INIT( 0 ); 

  //! buzzier trigger flag
  static atomic_t trig_bzr = ATOMIC_INIT( 0 ); 

  //! led state
  static u8_t led_state;

  //! motor state
  static u8_t mtr_state;

  //! buzzier state
  static u8_t bzr_state;
  
  //! GPIO device reference
  static struct device *gpio_dev;
  
  /// @brief function that initializes vest control unit
  /// @return if the initialization is OK, return 0. Otherwise, return negative number.
  s16_t initVCU( void );
  
  /// @function that is called when the client wants to read buffer
  /// @param conn - connection instance
  /// @param attr - GATT attribute which is related to read vest service
  /// @param buf - buffer
  /// @param len - buffer length
  /// @param offset - buffer offset
  /// @return if read is success, return read byte(s). Otherwise, return 0.
  ssize_t readVCU( struct bt_conn *conn,
		 const struct bt_gatt_attr *attr,
		 void *buf,
		 u16_t len,
		 u16_t offset );
  
  /// @function that is called when the client wants to write buffer
  /// @param conn - connection instance
  /// @param attr - GATT attribute which is related to write vest service
  /// @param buf - buffer
  /// @param len - buffer length
  /// @param offset - buffer offset
  /// @param flag - ?
  /// @return if write is success, return written byte(s). Otherwise, return 0.
  ssize_t writeVCU( struct bt_conn *conn,
		  const struct bt_gatt_attr *attr,
		  const void *buf,
		  u16_t len,
		  u16_t offset,
		  u8_t flags );
  

  /// @brief function that emits LED light during a period of time
  /// @return -
  void emitter( void );

  /// @brief function that actuates vibration motor during a period of time
  /// @return -
  void actuator( void );

  /// @brief function that triggers buzzier during a period of time
  /// @return -
  void buzzier( void );
  
  //! vest control unit attribute list
  static struct bt_gatt_attr vcu_attrs[] = {
    BT_GATT_PRIMARY_SERVICE( BT_UUID_VEST ),
        
    BT_GATT_CHARACTERISTIC( BT_UUID_VEST_IO,
			    BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
			    BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			    readVCU,
			    writeVCU,
			    &vcu_data )
  };

#ifdef __cplusplus
}
#endif

#endif
