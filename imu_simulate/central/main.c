/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define SYS_LOG_LEVEL (4)

#include <zephyr/types.h>
#include <stddef.h>
#include <errno.h>
#include <zephyr.h>
#include <misc/printk.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include <misc/byteorder.h>
#include <board.h>
#include <device.h>
#include <gpio.h>

//! to use user-defined service instead of Heart Rate Service
#define USER_DEFINED_SERVICE

//! led port
#define LED_PORT ( "GPIO_0" )

//! led id
#define LED ( 5 )

//! user-defined service UUID length
#define USERDEF_SRV_UUID_LEN ( 16 )

// weapon control unit UUID
#define BT_UUID_WCU  BT_UUID_DECLARE_128( 0x51, 0x72, 0x69, 0x6d, \
					  0x61, 0x72, 0x79, 0x53, \
					  0x65, 0x72, 0x76, 0x69, \
					  0x63, 0x65, 0x49, 0x64 )

// IMU sensor UUID
#define BT_UUID_IMU  BT_UUID_DECLARE_128( 0x49, 0x4d, 0x55, 0x53, \
					  0x65, 0x6e, 0x73, 0x53, \
					  0x65, 0x72, 0x76, 0x69, \
					  0x63, 0x65, 0x49, 0x64 )

// command UUID
#define BT_UUID_COMM  BT_UUID_DECLARE_128( 0x43, 0x6f, 0x6d, 0x6d, \
					   0x61, 0x6e, 0x64, 0x53, \
					   0x65, 0x72, 0x76, 0x69, \
					   0x63, 0x65, 0x49, 0x64 )

//! primary service uuid
static struct bt_uuid_128 *wcu_serv_uuid;
//! IMU data service UUID
static struct bt_uuid_128 *imu_serv_uuid;
//! command service UUID
static struct bt_uuid_128 *comm_serv_uuid;

//! connection descriptor
static struct bt_conn *default_conn;

#ifdef USER_DEFINED_SERVICE
static struct bt_uuid_128 uuid = BT_UUID_INIT_128( 0x00 );
#else
static struct bt_uuid_16 uuid = BT_UUID_INIT_16( 0x00 );
#endif

//! discovery parameters instance
static struct bt_gatt_discover_params discover_params;
//! subscribe parameters instance
static struct bt_gatt_subscribe_params subscribe_params;

//! GPIO device descriptor
struct device *gpio_dev;

/// @brief function that is called when peripheral notified
/// @param
/// @param
/// @param
/// @param
/// @return -
static u8_t notify_func(struct bt_conn *conn,
			struct bt_gatt_subscribe_params *params,
			const void *data, u16_t length)
{
  // if peripheral does not send data, do not continue
  if( !data ) {
    printk( "[UNSUBSCRIBED]\n" );
    params->value_handle = 0;
    return BT_GATT_ITER_STOP;
  }

  printk( "[NOTIFICATION] length %u : ", length );

  for( int i=0 ; i < length ; ++i ) {
    printk( "%d ", (( u8_t * ) data)[i] );
  }

  printk( "\n" );

  return BT_GATT_ITER_CONTINUE;
}

static u8_t discover_func( struct bt_conn *conn,
			   const struct bt_gatt_attr *attr,
			   struct bt_gatt_discover_params *params)
{
  int err = 0;

  if( !attr ) {
    printk( "Discover complete, attr is NULL\n" );   
    memset( params, 0, sizeof( *params ) );
    
    return BT_GATT_ITER_STOP;
  }

  printk( "[ATTRIBUTE] handle %u\n", attr->handle );

  if( !bt_uuid_cmp( discover_params.uuid, BT_UUID_WCU ) ) {
    memcpy( &uuid, BT_UUID_IMU, sizeof( uuid ) );
    
    discover_params.uuid = &uuid.uuid;
    discover_params.start_handle = attr->handle + 1;
    discover_params.type = BT_GATT_DISCOVER_CHARACTERISTIC;

    err = bt_gatt_discover( conn, &discover_params );
    
    if( err ) {
      printk( "Discover failed (err %d)\n", err );
    }
    else {
      printk( "discover_func BT_UUID_WCU\n" );
    }
  }
  else if( !bt_uuid_cmp(discover_params.uuid, BT_UUID_IMU ) ) {
    memcpy(&uuid, BT_UUID_GATT_CCC, sizeof(uuid));

    discover_params.uuid = &uuid.uuid;
    discover_params.start_handle = attr->handle + 2;
    discover_params.type = BT_GATT_DISCOVER_DESCRIPTOR;
    subscribe_params.value_handle = attr->handle + 1;

    err = bt_gatt_discover(conn, &discover_params);
    
    if (err) {
      printk("Discover failed (err %d)\n", err);
    }
    else {
      printk( "discover_func BT_UUID_IMU\n" );
    }
  }
  else {
    subscribe_params.notify = notify_func;
    subscribe_params.value = BT_GATT_CCC_NOTIFY;
    subscribe_params.ccc_handle = attr->handle;

    err = bt_gatt_subscribe(conn, &subscribe_params);
    
    if (err && err != -EALREADY) {
      printk("Subscribe failed (err %d)\n", err);
    } else {
      printk("[SUBSCRIBED]\n");
    }

    return BT_GATT_ITER_STOP;
  }

  return BT_GATT_ITER_STOP;
}

static bool eir_found( struct bt_data *data, void *user_data ) {
  bt_addr_le_t *addr = user_data;
  int i;
  char dev[ BT_ADDR_LE_STR_LEN ] = { 0x00 };

  bt_addr_le_to_str( addr, dev, sizeof( dev ) );
  
  printk( " [AD]: Type   %02u\n", data->type );
  printk( " [AD]: Length %02u\n", data->data_len );
  printk( " [AD]: Data   " );
  
  for( i=0 ; i < data->data_len ; ++i ) {
    printk( "0x%02x ", data->data[i] );
  }

  printk( "\n\n" );
  
  switch( data->type ) {
    #ifndef USER_DEFINED_SERVICE
  case BT_DATA_UUID16_SOME:
  case BT_DATA_UUID16_ALL: {
    if( data->data_len % sizeof( u16_t ) != 0 ) {
      printk( "AD malformed\n" );
      return true;
    }

    for( i = 0; i < data->data_len; i += sizeof( u16_t ) ) {
      struct bt_uuid *uuid;
      u16_t u16;
      int err;

      memcpy( &u16, &data->data[i], sizeof( u16 ) );
      uuid = BT_UUID_DECLARE_16( sys_le16_to_cpu( u16 ) );

      // if received UUID equals defined advertising UUID, that means matching is correct
      if( bt_uuid_cmp( uuid, BT_UUID_HRS ) ) {
	continue;
      }
	
      err = bt_le_scan_stop();
      
      if( err ) {
	printk( "Stop LE scan failed (err %d)\n", err );
	continue;
      }

      default_conn = bt_conn_create_le( addr, BT_LE_CONN_PARAM_DEFAULT );
      return false;
    }

    return true;
  }
    #else
  case BT_DATA_UUID128_SOME:
  case BT_DATA_UUID128_ALL: {
    if( data->data_len % USERDEF_SRV_UUID_LEN != 0 ) {
      printk( "AD malformed\n" );
      return true;
    }

    struct bt_uuid_128 recv_uuid;
    int err = 0;

    recv_uuid.uuid.type = BT_UUID_TYPE_128;
    memcpy( recv_uuid.val, data->data, USERDEF_SRV_UUID_LEN );

    // if received UUID equals defined advertising UUID, that means matching is correct
    if( bt_uuid_cmp( &recv_uuid.uuid, BT_UUID_WCU ) ) {
      return true;
    }

    err = bt_le_scan_stop();
          
    if( err ) {
      printk( "Stop LE scan failed (err %d)\n", err );
      return true;
    }
    
    default_conn = bt_conn_create_le( addr, BT_LE_CONN_PARAM_DEFAULT );
      
    if( NULL != default_conn ) {
      printk( "A connection is established for WCU advertiser\n" );
      return false;
    }
  }
#endif

  }

  return true;
}

static void device_found( const bt_addr_le_t *addr, s8_t rssi,
			  u8_t type, struct net_buf_simple *ad )
{
  char dev[ BT_ADDR_LE_STR_LEN ] = { 0x00 };
   
  /* connect only to devices in close proximity */
  if (rssi < -70) {
    return;
  }

  bt_addr_le_to_str( addr, dev, sizeof( dev ) );

  printk( " [DEVICE]: %s, AD evt type %u, AD data len %u, RSSI %i\n",
	 dev, type, ad->len, rssi );

  /* We're only interested in connectable events */
  if( type == BT_LE_ADV_IND || type == BT_LE_ADV_DIRECT_IND ) {
    bt_data_parse( ad, eir_found, ( void * ) addr );
  }
  else {
    printk( "Device Found Data Type: %d\n", type );
  }

  return;
}

// connected function
static void connected( struct bt_conn *conn, u8_t conn_err ) {
  char addr[ BT_ADDR_LE_STR_LEN ] = { 0x00 };
  int err;

  // convert peripheral address to string
  bt_addr_le_to_str( bt_conn_get_dst( conn ), addr, sizeof( addr ) );

  if( conn_err ) {
    printk( "Failed to connect to %s (%u)\n", addr, conn_err );
    return;
  }

  printk( "Connected: %s\n", addr );
  gpio_pin_write( gpio_dev, LED, 1 );

  if( conn == default_conn ) {
    #ifdef USER_DEFINED_SERVICE
    memcpy( &uuid, BT_UUID_WCU, sizeof( uuid ) );
    #else
    memcpy( &uuid, BT_UUID_HRS, sizeof( uuid ) );
    #endif

    printk( "UUID Type %d, UUID Val: ", uuid.uuid.type );

    for( int i=0 ; i < 16 ; ++i ) {
      printk( "0x%02x ", uuid.val[i] );
    }

    printk( "\n" );
    
    discover_params.uuid = &uuid.uuid;
    discover_params.func = discover_func;
    discover_params.start_handle = 0x0001;
    discover_params.end_handle = 0xffff;
    discover_params.type = BT_GATT_DISCOVER_PRIMARY;

    // call discover function to discover server attributes
    err = bt_gatt_discover( default_conn, &discover_params );
    
    if( err ) {
      printk( "Discover failed(err %d)\n", err );
    }
  }

  return;
}

// disconnected function
static void disconnected( struct bt_conn *conn, u8_t reason ) {
  char addr[ BT_ADDR_LE_STR_LEN ] = { 0x00 };
  int err;

  // convert peripheral address to string
  bt_addr_le_to_str( bt_conn_get_dst( conn ), addr, sizeof( addr ) );

  printk( "Disconnected: %s (reason %u)\n", addr, reason );

  if( default_conn != conn ) {
    return;
  }

  // unreference connection descriptor
  bt_conn_unref( default_conn );
  default_conn = NULL;

  // close LED
  gpio_pin_write( gpio_dev, LED, 0 );
  
  /* This demo doesn't require active scan */
  err = bt_le_scan_start(BT_LE_SCAN_PASSIVE, device_found );
  if( err ) {
    printk("Scanning failed to start (err %d)\n", err);
  }

  return;
}

// callback structure instance
static struct bt_conn_cb conn_callbacks = {
  .connected = connected,
  .disconnected = disconnected,
};

// main function
void main( void ) {
  int err = 0;

  memcpy( &wcu_serv_uuid, BT_UUID_WCU, sizeof( wcu_serv_uuid ) );
  memcpy( &imu_serv_uuid, BT_UUID_IMU, sizeof( imu_serv_uuid ) );
  memcpy( &comm_serv_uuid, BT_UUID_COMM, sizeof( comm_serv_uuid ) );
    
  gpio_dev = device_get_binding( LED_PORT );

  if( NULL == gpio_dev ) {
    printk( "GPIO device not found\n" );
    return;
  }

  gpio_pin_configure( gpio_dev, LED, GPIO_DIR_OUT );

  // enable bluetooth, no need function callback for central
  err = bt_enable( NULL );

  if( err ) {
    printk( "Bluetooth init failed (err %d)\n", err );
    return;
  }

  printk( "\nBluetooth initialized\n" );

  // register connected and disconnected callbacks
  bt_conn_cb_register( &conn_callbacks );

  // start scanning as active
  err = bt_le_scan_start( BT_LE_SCAN_ACTIVE, device_found );

  if( err ) {
    printk( "Scanning failed to start (err %d)\n", err );
    return;
  }

  printk( "Scanning successfully started\n\n" );

  return;
}
