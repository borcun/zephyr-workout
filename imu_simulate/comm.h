/** @file imu.h
 *  @brief IMU Service sample
 */

#ifdef __cplusplus
extern "C" {
#endif

  /// @brief function that initializes IMU service
  /// @return -
  void imu_init( void );

  /// @function that send IMU data to client when notification is active
  /// @return -
  void imu_notify( void );
  
#ifdef __cplusplus
}
#endif
