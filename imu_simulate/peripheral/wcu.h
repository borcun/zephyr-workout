/** @file wcu.h
 *  @brief Weapon Control Unit
 */

#ifndef WEAPON_CONTROL_UNIT_H
#define WEAPON_CONTROL_UNIT_H

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <misc/printk.h>
#include <misc/byteorder.h>
#include <zephyr.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

//! stack size of simulation thread
#define STACK_SIZE ( 512 )

//! priority value of simulation thread
#define PRIORITY ( 7 )

//! IMU data length
#define IMU_DATA_LEN ( 12 )

// weapon control unit UUID
#define BT_UUID_WCU  BT_UUID_DECLARE_128( 0x51, 0x72, 0x69, 0x6d, \
					  0x61, 0x72, 0x79, 0x53, \
					  0x65, 0x72, 0x76, 0x69, \
					  0x63, 0x65, 0x49, 0x64 )

// IMU sensor UUID
#define BT_UUID_IMU  BT_UUID_DECLARE_128( 0x49, 0x4d, 0x55, 0x53, \
					  0x65, 0x6e, 0x73, 0x53, \
					  0x65, 0x72, 0x76, 0x69, \
					  0x63, 0x65, 0x49, 0x64 )

// command UUID
#define BT_UUID_COMM  BT_UUID_DECLARE_128( 0x43, 0x6f, 0x6d, 0x6d, \
					   0x61, 0x6e, 0x64, 0x53, \
					   0x65, 0x72, 0x76, 0x69, \
					   0x63, 0x65, 0x49, 0x64 )

#ifdef __cplusplus
extern "C" {
#endif
  /** 
   * @union imu_data_t
   * @brief IMU data structure
   */
  typedef union {
    struct {
      //! theta angle
      u16_t theta;
      //! phi angle
      u16_t phi;
      //! psi angle
      u16_t psi; 
      //! acceleration x
      u16_t acc_x;
      //! acceleration y
      u16_t acc_y;
      //! acceleration z
      u16_t acc_z;
    };

    //! total IMU data
    u8_t data[ IMU_DATA_LEN ];
    
  }  imu_t;

  //! IMU data structure instance
  static imu_t imu = { .data = { 0x00 } };

  //! command
  static u8_t command = 0;
  
  //! client configure data
  static struct bt_gatt_ccc_cfg imuc_ccc_cfg[ BT_GATT_CCC_MAX ] = {};

  //! simulate IMU flag
  static u8_t simulate_imu = 0;

  //! simulation semaphore
  static struct k_sem sim_sem;

  /// @brief function that initializes weapon control unit
  /// @return -
  void initWCU( void );

  /// @function that send IMU data to client when notification is active
  /// @return -
  u8_t onNotifiedIMU( void );

  /// @function that is called when client configuration is changed
  /// @remark this function is called when notification is enabled/disabled
  /// @param attr - attribute which is related to CCC
  /// @param value - changed value
  /// @return
  void onChangedCCC( const struct bt_gatt_attr *attr,
	       u16_t value );
  
  /// @function that is called when the client wants to read buffer
  /// @param conn - connection instance
  /// @param attr - GATT attribute which is related to read command service
  /// @param buf - buffer
  /// @param len - buffer length
  /// @param offset - buffer offset
  /// @return if read is success, return read byte(s). Otherwise, return 0.
  ssize_t readComm( struct bt_conn *conn,
		    const struct bt_gatt_attr *attr,
		    void *buf,
		    u16_t len,
		    u16_t offset );

  /// @function that is called when the client wants to write buffer
  /// @param conn - connection instance
  /// @param attr - GATT attribute which is related to write command service
  /// @param buf - buffer
  /// @param len - buffer length
  /// @param offset - buffer offset
  /// @param flag - ?
  /// @return if write is success, return written byte(s). Otherwise, return 0.
  ssize_t writeComm( struct bt_conn *conn,
		     const struct bt_gatt_attr *attr,
		     const void *buf,
		     u16_t len,
		     u16_t offset,
		     u8_t flags );
  
  /// @brief thread function which is called peridiocally
  /// @return -
  void simulate( void );
  
  //! WCU attribute list
  static struct bt_gatt_attr wcu_attrs[] = {
    BT_GATT_PRIMARY_SERVICE( BT_UUID_WCU ),
    
    BT_GATT_CHARACTERISTIC( BT_UUID_IMU,
			    BT_GATT_CHRC_NOTIFY,
			    BT_GATT_PERM_READ,
			    NULL,
			    NULL,
			    NULL ),
    
    BT_GATT_CCC( imuc_ccc_cfg, onChangedCCC ),
    
    BT_GATT_CUD( "IMU-Sensor", BT_GATT_PERM_READ ),
    
    BT_GATT_CHARACTERISTIC( BT_UUID_COMM,
			    BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
			    BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			    readComm,
			    writeComm,
			    &command ),
    
    BT_GATT_CUD( "Player-Command", BT_GATT_PERM_READ )
  };  

#ifdef __cplusplus
}
#endif

#endif
