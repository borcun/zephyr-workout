#include "wcu.h"

//! wcu service
struct bt_gatt_service wcu_svc = BT_GATT_SERVICE( wcu_attrs );

void onChangedCCC( const struct bt_gatt_attr *attr, u16_t value ) {
  simulate_imu = ( value == BT_GATT_CCC_NOTIFY ) ? 1 : 0;
    
  return;
}

ssize_t readComm(struct bt_conn *conn, const struct bt_gatt_attr *attr,
		 void *buf, u16_t len, u16_t offset )
{
  ssize_t res = bt_gatt_attr_read( conn, attr, buf, len, offset,
				   &command, sizeof( command ) );

  if( 0 < res ) {
    printk( "Client queried current command value, it is %d\n", command );
  }
  else {
    printk( "Client queried current command, query failed\n" );
  }
  
  return res;
}


ssize_t writeComm( struct bt_conn *conn, const struct bt_gatt_attr *attr,
		   const void *buf, u16_t len, u16_t offset, u8_t flags)
{
  u8_t *value = attr->user_data;
  
  memcpy( value, buf, len );
  command = *value;

  switch( command ) {
  case 1: { printk( "Start Scenario\n" ); break; }
  case 2: { printk( "Stop Scenario\n" ); break; }
  default: { printk( "Invalid Command\n" ); break; }
  }
  
  return len;
}

void initWCU( void ) { 
  bt_gatt_service_register( &wcu_svc );
}

u8_t onNotifiedIMU( void ) {
  // if notification is closed by client, do not process
  if( !simulate_imu ) {
    return simulate_imu;
  }

  k_sem_take( &sim_sem, 200 );
  s8_t res = bt_gatt_notify( NULL, &wcu_attrs[1], &imu.data, sizeof( imu.data ) );
  k_sem_give( &sim_sem );
  
  if( 0 != res ) {
    printk( "bt_gatt_notify failed: %d\n", res );
  }
  
  return res;
}

// thread function
void simulate( void ) {
  k_sem_init( &sim_sem, 0, UINT_MAX );
  
  while( 1 ) {
    k_sem_take( &sim_sem, K_FOREVER );
    
    imu.psi += 1;
    imu.phi += 1;
    imu.theta += 1;
    imu.acc_x += 1;
    imu.acc_y += 1;
    imu.acc_z += 1;

    k_sem_give( &sim_sem );
    
    k_sleep( MSEC_PER_SEC );	
  }

  return;
}

K_THREAD_DEFINE( imu_thread,
		 STACK_SIZE,
		 simulate,
		 NULL,
		 NULL,
		 NULL,
		 PRIORITY,
		 0,
		 K_NO_WAIT);
